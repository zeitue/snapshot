#!/bin/bash

sudo install -o root -m 755 -D -v ./snapshot.sh -t /etc/snapshot
sudo install -o root -m 444 -D -v ./snapshot.conf -t /etc/snapshot
sudo install -o root -m 444 -D -v conf.d/* -t /etc/snapshot/conf.d

