#!/bin/bash

PREFIX="/etc/snapshot"
source "$PREFIX/snapshot.conf"
TARGET="$TARGET_USER@$TARGET_HOST"
KEEP="+$((KEEP_COUNT+1))"


SCRIPT_NAME="$0"
PID_FILE=/tmp/snapshot-pid
DATASETS_G1=(${SOURCE_DATASETS//:/ })
DATASETS_G2=(${SOURCE_POOLS//:/ })
DATASETS_G3=$PREFIX/conf.d
PID=$(pgrep -f "$SCRIPT_NAME")

IFS=$'\n'
DR1=($(find ${DATASETS_G1[@]} -maxdepth 1 -mindepth 1 -name 'snapshot.conf' -type f))
DR2=($(find ${DATASETS_G2[@]} -maxdepth 2 -mindepth 2 -name 'snapshot.conf' -type f))
DR3=($(find ${DATASETS_G3[@]} -name '*.conf' -type f))
unset IFS

DATASETS=(${DR1[@]} ${DR2[@]} ${DR3[@]})

function stop_containers() {
  for container in "$@"; do
    echo docker stop "$container"
  done
}

function start_containers() {
  for container in "$@"; do
    docker start "$container"
  done
}

function take_snapshot() {
  DS="$1"
  SNAPDATE=$(date +%Y-%m-%d_%H:%M:%S)
  zfs snapshot "$DS@$SNAPDATE"
}

function send_snapshots () {
  DS="$1"
  TS="$2"
  LAST_SNAP=$(ssh $TARGET zfs list -t snapshot -o name -s creation | grep "^$TS" | tac | head -n1)
  CURR_SNAP=$(zfs list -t snapshot -o name -s creation | grep "^$DS" | tac | head -n1)
  if [[ -z $LAST_SNAP ]]; then
    zfs send "$CURR_SNAP" | ssh "$TARGET" sudo zfs recv -F "$TS"
  else
    LAST_SNAP=$(echo $LAST_SNAP | sed "s#${LAST_SNAP%@*}#${CURR_SNAP%@*}#g")
    zfs send -i "$LAST_SNAP" "$CURR_SNAP" | ssh "$TARGET" sudo zfs recv -F "$TS"
  fi
}

function clean_old_snapshots() {
  DS="$1"
  TS="$2"
  # clean up locale
  OLD_SNAPS=$(zfs list -t snapshot -o name | grep "^$DS" | tac | tail -n "$KEEP")
  if [[ ! -z $OLD_SNAPS ]]; then
    echo "$OLD_SNAPS" | xargs -n 1 zfs destroy -r
  fi

  # clean up remote
  if [[ ! -z "$TARGET_HOST" ]]; then
    OLD_SNAPS=$(ssh "$TARGET" zfs list -t snapshot -o name | grep "^$TS" | tac | tail -n "$KEEP")
    if [[ ! -z $OLD_SNAPS ]]; then
      ssh "$TARGET" "echo \"$OLD_SNAPS\" | xargs -n 1 zfs destroy -r"
    fi
  fi
}



if [[ -f "$PID_FILE" ]]; then
  echo "$SCRIPT_NAME: Already running"
  exit 0
else
  echo "$PID" > "$PID_FILE"
  for CONF in ${DATASETS[@]}; do
    source "$CONF"
    if [ ! -z $DATASET ]; then

      # shutdown containers
      stop_containers ${CONTAINERS[@]}

      # take snapshot
      take_snapshot $DATASET

      # start containers
      start_containers ${CONTAINERS[@]}

      # send snapshots
      if [[ ! -z "$TARGET_HOST" ]]; then
        send_snapshots $DATASET $TARGET_DATASET
      fi

      # clean up old snapshots
      clean_old_snapshots $DATASET $TARGET_DATASET
    fi
  done
  rm "$PID_FILE"
fi
