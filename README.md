# snapshot

A system to keep snapshots of the server and back them up to a remote server

## Installation

run `./install.sh`

Note: You can place your configuration files into the `conf.d` directory before install and it will place them in the correct place.

## Configuration

You will need to set the values in the `snapshot.conf` file

```bash
# Snapshot config file

# The number of snapshots to keep
KEEP_COUNT=7

# Username of user to run the backup commands
TARGET_USER=superuser

# The ip address or hostname of destination server to send backups
TARGET_HOST=backup-server

# List of pools containing datasets, seperated by :
SOURCE_POOLS=/data:/mnt/important

# List of datasets, seperated by :
SOURCE_DATASETS=/work/gitea:/work/openproject


```

For each dataset you want to snapshot you'll need to create one configuration. 

Configuration files can be done in two different ways

1. create a configuration file called `dataset.conf` where `dataset` is your dataset name not including the pool
2. create a configuration file called `snapshot.conf` and place it in the dataset directory (make sure you have `SOURCE_POOLS` or `SOURCE_DATASETS` varialbe is set)

The contents of the configuration files are very simple, as follows:

```bash
# containers to shutdown during snapshot, such as databases
CONTAINERS=(container_name)

# dataset to snapshot
DATASET=pool/dataset

# dataset on target to send to
TARGET_DATASET=pool/dataset

```



## Example

`gitea.conf` or `gitea/snapshot.conf`

```bash
# containers
CONTAINERS=(gitea gitea-db)

# dataset
DATASET=data/gitea

# dataset on target to send to
TARGET_DATASET=backup/gitea

```



## Scheduling

You can schedule snapshot to run using `cron` calling `crontab -e` as the desired user and then enter the required time to run and include the path on the line above. The `PATH` variable needs to be set in the `crontab` because `zfs `is generally not in the `cron` path and won't be available to the script when run under `cron`.

Example `crontab`

```bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin

0 0  *   *   *     /etc/snapshot/snapshot.sh


```



